import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import {RouterModalAbortRetryIgnore} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-abort-retry-ignore',
    template: `
        <p>
            An error occurred while trying to apply changes. Click retry to try again, Ignore to skip or Abort to cancel.
        </p>
    `,
    styles: [`
    `]
})
export class AbortRetryIgnoreComponent extends RouterModalAbortRetryIgnore implements OnInit, OnDestroy {
    @Input() modalTitle = 'Operation Error';
    private subscription: Subscription;

    constructor(protected router: Router, protected activatedRoute: ActivatedRoute) {
        // call super constructor
        super(router, activatedRoute);
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    ngOnInit(): void {
        // get yes button text from route data parameters
        this.subscription = this.activatedRoute.data.subscribe( data => {
            //
        });
    }
    //

    async ignore() {
        // do something and close
        return this.close();
    }

    abort(): Promise<any> {
        // do something and close
        return this.close();
    }

    retry(): Promise<any> {
        // do something and close
        return this.close();
    }

}
