import { AfterViewInit, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { AdvancedFormsService, ServiceUrlPreProcessor } from '@universis/forms';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { cloneDeep } from 'lodash';
import { replacedByClassForm } from './replacedByClass.form';

@Component({
  selector: 'one-roster-replaced-by-class',
  template: `
  <!--pre-loading state-->
  <ng-template #loading>
    <div class="card shadow-none mb-0 mt-2">
        <div class="card-body">
          <div class="ph-col-12">
            <div class="ph-row">
              <div class="ph-col-8 big"></div>
            </div>
            <div class="ph-row">
              <div class="ph-col-8 big"></div>
            </div>
            <div class="ph-row">
              <div class="ph-col-2 big"></div>
              <div class="ml-4 ph-col-4 big"></div>
            </div>
            <div class="ph-row">
              <div class="ph-col-4 big"></div>
            </div>
          </div>
        </div>
    </div>
  </ng-template>
    <!--end pre-loading state-->
  <div class="card mb-0 shadow-none">
          <div class="card-body py-2">
            <div class="d-flex">
              <div>
                <h3 id="ReplacedByClass" class="card-title mb-1 font-weight-normal" [translate]="'OneRoster.ReplacedByClass'"></h3>
                <p class="card-text" innerHTML="{{ 'OneRoster.ReplacedByClassDescription' | translate }}">
              </div>
            </div>
            <div class="mt-3" *ngIf="(replaces | async) as replaceClasses">
              <ng-container>
                  <h5 *ngIf="replaceClasses.length" class="my-3 text-success font-weight-normal" [translate]="'OneRoster.CannotBeReplacedByMessage'"></h5>
              </ng-container>
              <ng-container *ngIf="replaceClasses.length === 0">
                <ng-container *ngIf="isLoading">
                  <ng-container *ngTemplateOutlet="loading"></ng-container>
                </ng-container>
                <ng-container>
                  <!-- load form in the background -->
                  <div [ngClass]="{ 'd-none': isLoading || replaceClasses.length }">
                    <formio (formLoad)="onFormLoad()" (customEvent)="onEvent($event)" [renderOptions]="renderOptions" [form]="form" #formComponent [submission]="{ data: data}" ></formio>                  
                  </div>
                </ng-container>
              </ng-container>
            </div>
            
          </div>
        </div>
  `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./one-roster-class-configuration.component.scss']
})
export class OneRosterReplacedByClassComponent implements OnChanges, AfterViewInit, OnDestroy {

  public form: any;
  public data: any = {};
  private previous: any = {};
  public renderOptions: {
    language: string;
    i18n: any;
  };

  @Input('courseClass') courseClass: { id: string, title: string, department: string, displayCode: string, year: string, period: string };
  @Input('replaces') replaces: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

  public replacedBy$: BehaviorSubject<any> = new BehaviorSubject<any>({
    value: null
  });
  
  isLoading: boolean = true;

  private get replacedBy$$(): Observable<null> {
    return from(
      this.context.model('OneRosterReplaceClasses').where('courseClass').equal(this.courseClass.id)
      .expand('replacedBy($select=id,title,department/id as department,course/displayCode as displayCode)').getItem()
    )
  }

  subscription: any;

  constructor(
    private context: AngularDataContext,
    private activatedRoute: ActivatedRoute, 
    private errorService: ErrorService,
    private loadingService: LoadingService,
    private formService: AdvancedFormsService,
    private _translateService: TranslateService
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.courseClass && changes.courseClass.currentValue) {
      this.replacedBy$$.subscribe((item: any) => {
        // load form
        const form = cloneDeep(replacedByClassForm);
        new ServiceUrlPreProcessor(this.context).parse(form);
          const { currentLang: language } = this._translateService;
          this.renderOptions = {
            language,
            i18n: {
              [language]: form.settings.i18n[language]
            }
          }
          this.data = {
            courseClass: changes.courseClass.currentValue,
            replacedBy: item && item.replacedBy
          }
          this.replacedBy$.next({
            value: item
          });
          this.form = form;
      })
    }
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.replaces) {
      this.replaces.unsubscribe();
    }
  }
  ngAfterViewInit(): void {
    
  }

  onEvent(event: any) {
    if (event.type === 'replace') {
      this.loadingService.showLoading();
      const item = {
        courseClass: this.courseClass.id,
        replacedBy: event.data.replacedBy === '' ? null : event.data.replacedBy
      };
      (() => {
        if (item.replacedBy == null) {
          return this.remove();
        }
        return this.save(item);
      })().then(() => {
        this.loadingService.hideLoading();
        if (item.replacedBy == null) {
          this.replacedBy$.next({
            value: null
          });
        } else {
          this.replacedBy$.next({
            value: item
          });
        }
      }).catch((err) => {
        this.loadingService.hideLoading();
        this.errorService.showError(err, {
          continueLink: '.'
        });
      })
    }
  }

  remove() {
    return this.replacedBy$$.toPromise().then((item: any) => {
      if (!item) {
        return;
      }
      return this.context.model('OneRosterReplaceClasses').remove(item);
    });
  }

  save(item: { courseClass: any, replacedBy: any }) {
    return this.context.model('OneRosterReplaceClasses').save(item);
  }

  onFormLoad() {
    setTimeout(() => {
      this.isLoading = false;
    }, 500)
    
  }


}