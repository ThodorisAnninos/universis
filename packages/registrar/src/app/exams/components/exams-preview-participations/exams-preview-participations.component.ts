import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { Subscription, combineLatest } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';
import {map} from 'rxjs/operators';
import {SearchConfigurationResolver, TableConfigurationResolver} from '../../../registrar-shared/table-configuration.resolvers';

@Component({
  selector: 'app-exams-preview-participations',
  templateUrl: './exams-preview-participations.component.html'
})
export class ExamsPreviewParticipationsComponent implements OnInit, OnDestroy {

  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public examId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _context: AngularDataContext,
    private _activeDepartmentService: ActiveDepartmentService,
    private _tableResolver: TableConfigurationResolver,
    private _searchResolver: SearchConfigurationResolver
  ) { }

  async ngOnInit() {
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this.subscription = combineLatest(
      this._activatedRoute.params,
      this._tableResolver.get('CourseExamParticipateActions', 'list'),
      this._searchResolver.get('CourseExamParticipateActions', 'list')
    ).pipe(
      map(([params, tableConfiguration, searchConfiguration]) => ({ params, tableConfiguration, searchConfiguration }))
    ).subscribe(async (results) => {
      this._activatedTable.activeTable = this.students;
      this.examId = results.params.id;

      this.students.query = this._context.model('CourseExamParticipateActions')
        .where('courseExam').equal(results.params.id)
        .prepare();

      this.students.config = AdvancedTableConfiguration.cast(results.tableConfiguration);
      this.students.fetch();

      if (results.searchConfiguration) {
        this.search.form = results.searchConfiguration;
        this.search.formComponent.formLoad.subscribe((res: any) => {
          Object.assign(res, { department: activeDepartment });
        });
        this.search.ngOnInit();
      }
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.students.fetch(true);
      }
    });
  }
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
