import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient) { }

  getRequestById(request) {
    return this._context.model('StudentRequestActions')
      .asQueryable()
      .where('id').equal(request)
      .expand(
          'agent',
          'student($expand=person, studentStatus, inscriptionYear)',
          'messages($orderby=dateCreated desc;$expand=attachments)'
      )
      .getItem();
  }

  getRequestDocumentById(request) {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('id').equal(request)
      .expand( 'object($expand=reportTemplate), result($expand=documentStatus), student($expand=person, studentStatus, inscriptionYear),messages($orderby=dateCreated desc;$expand=attachments)')
      .getItem();
  }

  getRequestMessageById(request) {
    return this._context.model('RequestMessageActions')
      .asQueryable()
      .where('id').equal(request)
      .expand( 'student($expand=person, studentStatus, inscriptionYear),messages($orderby=dateCreated desc;$expand=attachments)')
      .getItem();
  }

  getRequestExamParticipate(request) {
    return this._context.model('examPeriodParticipateActions')
      .asQueryable()
      .where('id').equal(request)
      .expand('student($expand=person, studentStatus, inscriptionYear),messages($orderby=dateCreated desc;$expand=attachments), courseExamActions')
      .getItem();
  }

   changeRequestStatus(request, status): any {
    // set request status
    request.actionStatus = {'alternateName': status.alternateName};
    // get request model from additionalType
    return this._context.getMetadata().then(metadata => {
      const entitySet = metadata.EntityContainer.EntitySet.find(x => {
        return x.EntityType === request.additionalType;
      });
      const requestModel = entitySet ? entitySet.Name : 'StudentRequestActions';
      return this._context.model(requestModel)
        .save(request);
    });
  }

  changePublishedStatus(result): any {
    return this._context.model('DocumentNumberSeriesItems')
      .save(result);
  }
  sendResponse(studentId, actionId, responseModel) {

    const formData: FormData = new FormData();
    if (responseModel.attachment) {
      formData.append('attachment', responseModel.attachment, responseModel.attachment.name);
    }
    formData.append('action', actionId);
    formData.append('subject', responseModel.subject);
    formData.append('body', responseModel.body);

    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`students/${studentId}/messages/send`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    });
  }

  downloadFile(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');

    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {

        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  getActionStatusTypes() {
    return this._context.model('ActionStatusTypes')
      .asQueryable()
      .where('alternateName').equal('CompletedActionStatus')
      .or('alternateName').equal('CancelledActionStatus')
      .or('alternateName').equal('ActiveActionStatus')
      .getItems();
  }


  /**
   *
   * Fetches the available event status
   *
   */
  async getAvailableGraduationEvent(studentId) {
    return this._context.model(`students/${studentId}/availableGraduationEvents`)
      .asQueryable()
      .expand('graduationYear,graduationPeriod,location')
      .getItems();
  }
}
