import {Component, Input, OnInit} from '@angular/core';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-request-side-preview',
  templateUrl: './request-side-preview.component.html'
})
export class RequestSidePreviewComponent implements OnInit {

  @Input('request') request;

  public currentLang: any;

  constructor(private _configurationService: ConfigurationService) { }

  ngOnInit() {
    this.currentLang = this._configurationService.currentLocale;
  }

}
