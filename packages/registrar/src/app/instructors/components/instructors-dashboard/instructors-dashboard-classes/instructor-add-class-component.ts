import {Component, Directive, EventEmitter, Input, OnDestroy, OnInit} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {
    AdvancedTableModalBaseComponent,
    AdvancedTableModalBaseTemplate
} from '@universis/ngx-tables';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ErrorService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-instructor-add-class',
    template: AdvancedTableModalBaseTemplate
})
export class InstructorAddClassComponent extends AdvancedTableModalBaseComponent {

    @Input() instructor: any;

    constructor(_router: Router, _activatedRoute: ActivatedRoute,
                _context: AngularDataContext,
                private _errorService: ErrorService,
                private _toastService: ToastService,
                private _translateService: TranslateService,
                protected advancedFilterValueProvider: AdvancedFilterValueProvider,
                protected datePipe: DatePipe) {
        super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
        // set default title
        this.modalTitle = 'Instructors.AddClass';
    }

    hasInputs(): Array<string> {
        return [ 'instructor' ];
    }

    ok(): Promise<any> {
        // get selected items
        const selected = this.advancedTable.selected;
        let items = [];
        if (selected && selected.length > 0) {
            // try to add classes
            items = selected.map( courseClass => {
                return {
                    instructor: this.instructor,
                    courseClass: courseClass
                };
            });
            return this._context.model('CourseClassInstructors')
                .save(items)
                .then( result => {
                    // add toast message
                    this._toastService.show(
                        this._translateService.instant('Instructors.AddClassMessage.title'),
                        this._translateService.instant((items.length === 1 ?
                            'Instructors.AddClassMessage.one' : 'Instructors.AddClassMessage.many')
                        , { value: items.length })
                    );
                    return this.close({
                        fragment: 'reload',
                        skipLocationChange: true
                    });
                }).catch( err => {
                    this._errorService.showError(err, {
                        continueLink: '.'
                    });
                });
        }
        return this.close();
    }
}
