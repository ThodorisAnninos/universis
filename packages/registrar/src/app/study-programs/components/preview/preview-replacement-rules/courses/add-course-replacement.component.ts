import {Component, Input, OnInit} from '@angular/core';
import {AdvancedTableConfiguration, AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate} from '@universis/ngx-tables';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ErrorService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { AdvancedFilterValueProvider } from '@universis/ngx-tables';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-replacement-course',
    template: AdvancedTableModalBaseTemplate
})
export class AddCourseReplacementComponent extends AdvancedTableModalBaseComponent implements OnInit {

    @Input() rule: any;
    @Input() studyProgram: any;
    private paramSubscription: Subscription;
    private dataSubscription: Subscription;
    constructor(private _router: Router,
                private _activatedRoute: ActivatedRoute,
                protected _context: AngularDataContext,
                private _errorService: ErrorService,
                private _toastService: ToastService,
                private _translateService: TranslateService,
                protected advancedFilterValueProvider: AdvancedFilterValueProvider,
                protected datePipe: DatePipe) {
        super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
        // set default title
        this.modalTitle = 'StudyPrograms.AddCourse';
        // this.tableConfigSrc = 'assets/tables/ProgramCourses/config.list.json';
    }


    hasInputs(): Array<string> {
        return ['rule'];
    }
    ngOnInit() {
        this.dataSubscription = this._activatedRoute.data.subscribe(data => {
            // get query params
            if (this.paramSubscription) {
                this.paramSubscription.unsubscribe();
            }
            this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
                if (data.tableConfiguration) {
                    const tableConfiguration = AdvancedTableConfiguration.cast(data.tableConfiguration);
                    this.studyProgram = parseInt(params.id, 10) || 0;
                    this.rule = parseInt(params.rule, 10) || 0;
                    tableConfiguration.defaults.filter = `program eq ${this.studyProgram}`;
                    this.tableConfig = tableConfiguration;

                }
            });
        });
    }

    ok(): Promise<any> {
        // get selected items
        const selected = this.advancedTable.selected;
        let items = [];
        if (selected && selected.length > 0) {
            // try to add students
            items = selected.map( course => {
                return {
                    studyProgramReplacementRule: this.rule,
                    course: course.courseId
                };
            });
            return this._context.model('CourseReplacements')
                .save(items)
                .then( result => {
                    // add toast message
                    this._toastService.show(
                        this._translateService.instant('StudyPrograms.AddTransferCourses.title'),
                        this._translateService.instant((items.length === 1 ?
                                'StudyPrograms.AddTransferCourses.one' : 'StudyPrograms.AddTransferCourses.many')
                            , { value: items.length })
                    );
                    return this.close({
                        fragment: 'reload',
                        skipLocationChange: true
                    });
                }).catch( err => {
                    this._errorService.showError(err, {
                        continueLink: '.'
                    });
                });
        }
        return this.close();
    }
}
