import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-internships-preview-form',
  templateUrl: './internships-preview-form.component.html'
})
export class InternshipsPreviewFormComponent implements OnInit, OnDestroy {

  @Input() model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() { }

  ngOnDestroy(): void { }


}
