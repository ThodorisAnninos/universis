import { TableColumnConfiguration } from '@universis/ngx-tables';

export const INTERNSHIP_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: 'id',
    property: 'internshipId',
    title: 'Internships.Shared.Id',
    hidden: true,
    optional: true
  },
  {
    name: 'name',
    property: 'internshipName',
    title: 'Internships.Shared.InternshipSubject',
    hidden: true,
    optional: true
  },
  {
    name: 'startDate',
    property: 'startDate',
    title: 'Internships.Shared.InternshipStartDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'endDate',
    property: 'endDate',
    title: 'Internships.Shared.InternshipEndDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'dateCompleted',
    property: 'dateCompleted',
    title: 'Internships.Shared.InternshipDateCompleted',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'internshipDate',
    property: 'internshipDate',
    title: 'Internships.Shared.InternshipDate',
    formatter: 'DateTimeFormatter',
    formatString: 'short',
    hidden: true,
    optional: true
  },
  {
    name: 'department/abbreviation',
    property: 'departmentAbbreviation',
    title: 'Internships.Shared.InternshipDepartment',
    hidden: true,
    optional: true
  },
  {
    name: 'internshipYear/alternateName',
    property: 'yearAlternateName',
    title: 'Internships.Shared.InternshipYear',
    hidden: true,
    optional: true
  },
  {
    name: 'internshipPeriod/alternateName',
    property: 'internshipPeriodAlternateName',
    title: 'Internships.Shared.InternshipPeriod',
    formatter: 'TranslationFormatter',
    formatString: 'Periods.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'companyContact',
    property: 'companyContact',
    title: 'Internships.Shared.InternshipCompanyContact',
    hidden: true,
    optional: true
  },
  {
    name: 'companyContactPhone',
    property: 'companyContactPhone',
    title: 'Internships.Shared.InternshipCompanyContactPhone',
    hidden: true,
    optional: true
  },
  {
    name: 'company/name',
    property: 'companyName',
    title: 'Internships.Shared.InternshipCompany',
    hidden: true,
    optional: true
  },
]
