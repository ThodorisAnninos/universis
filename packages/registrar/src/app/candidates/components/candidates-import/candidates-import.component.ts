import { HttpClient } from "@angular/common/http";
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AngularDataContext } from "@themost/angular";
import { LoadingService, ToastService } from "@universis/common";
import { AdvancedFormComponent } from "@universis/forms";
import { BehaviorSubject, Subscription } from "rxjs";

@Component({
  selector: "app-candidates-import",
  templateUrl: "./candidates-import.component.html",
  styleUrls: ["./candidates-import.component.scss"],
})
export class CandidatesImportComponent implements OnInit, OnDestroy {
  public lastError: any;
  public importBtnDisabled: boolean = true;
  public files = [];
  private formChangeSubscription: Subscription;
  private subject$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  @ViewChild("formComponent") formComponent: AdvancedFormComponent;

  constructor(
    private _loadingService: LoadingService,
    private _context: AngularDataContext,
    private _http: HttpClient,
    private _toastService: ToastService,
    private _translateService: TranslateService,
    private _router: Router
  ) {}

  ngOnInit() {
    this.subject$.subscribe(event => {
      if (event) {
        setTimeout(() => {
          this.formChangeSubscription = this.formComponent.form.change.subscribe(
            (event) => {
              if (Object.prototype.hasOwnProperty.call(event, "isValid")) {
                // enable or disable button based on form status
                this.importBtnDisabled = !(event.isValid && this.files.length);
              }
              // validate distinctly because of multiple formio change emits (first load)
              const candidateSource =
                this.formComponent.form.formio &&
                this.formComponent.form.formio.data &&
                this.formComponent.form.formio.data.candidateSource;
              if (!candidateSource || Object.keys(candidateSource).length === 0) {
                this.importBtnDisabled = true;
              }
            }
          );
        }, 0);
      }
    });
  }

  onSelect(event: { addedFiles: File[] }) {
    this.lastError = null;
    if (this.files.length !== 0) {
      this.files.splice(0, 1);
    }
    const addedFile = event.addedFiles[0];
    this.files.push(addedFile);
    if (this.formComponent && this.formComponent.form) {
      this.formComponent.form.change.emit({ isValid: true });
    }
    this.subject$.next(true);
  }

  onRemove() {
    this.files = [];
    this.lastError = null;
    if (this.formComponent && this.formComponent.form) {
      this.formComponent.form.change.emit({ isValid: false });
    }
  }

  async import() {
    let importedSuccessfully = false;
    try {
      this._loadingService.showLoading();
      // clear last error
      this.lastError = null;
      // get candidate source
      const candidateSource =
        this.formComponent &&
        this.formComponent.form &&
        this.formComponent.form.formio &&
        this.formComponent.form.formio.data &&
        this.formComponent.form.formio.data.candidateSource;
      if (!candidateSource || Object.keys(candidateSource).length === 0) {
        return;
      }
      // get force update option
      const forceUpdate = this.formComponent.form.formio.data.forceUpdate;
      // and attachment
      const attachment = this.files[0];
      if (attachment == null) {
        return;
      }
      // and prepare the form data
      const formData: FormData = new FormData();
      // append the file
      formData.append("file", attachment, attachment.name || "attachment");
      // append source and force update option
      formData.append(
        "data",
        JSON.stringify({
          candidateSource: candidateSource,
          forceUpdate: forceUpdate,
        })
      );
      // and send the request
      const serviceHeaders = this._context.getService().getHeaders();
      const postUrl = this._context
        .getService()
        .resolve("CandidateStudents/importFromSource");
      await this._http
        .post(postUrl, formData, {
          headers: serviceHeaders,
        })
        .toPromise();
      // set flag
      importedSuccessfully = true;
    } catch (err) {
      this.lastError = err;
    } finally {
      this._loadingService.hideLoading();
      if (importedSuccessfully) {
        // show an informative toast message
        const toastTitle = this._translateService.instant(
          "Candidates.ImportAction.SuccessToast.Title"
        );
        const toastMessage = this._translateService.instant(
          "Candidates.ImportAction.SuccessToast.Message"
        );
        this._toastService.show(toastTitle, toastMessage);
        // and navigate to candidate upload actions list
        setTimeout(() => {
          this._router.navigate(['/candidate-students', 'upload-actions']);
        }, 750);
      }
    }
  }

  ngOnDestroy(): void {
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }
}
