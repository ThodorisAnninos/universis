import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CandidatesSharedModule } from './candidates.shared';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { RouterModalModule } from '@universis/common/routing';
import {RouterModule} from '@angular/router';
import {StudyProgramsSharedModule} from '../study-programs/study-programs.shared';
import {StudentsSharedModule} from '../students/students.shared';
import {BsDatepickerModule, ModalModule, TabsModule, TimepickerModule} from 'ngx-bootstrap';
import {ElementsModule} from '../elements/elements.module';
import {ChartsModule} from 'ng2-charts';
import {MessagesSharedModule} from '../messages/messages.shared';
import {SendMessageToStudentComponent} from '../messages/components/send-message-to-student/send-message-to-student.component';
import {CandidatesTableComponent} from './components/candidates/candidates-table.component';
import {CandidatesRoutingModule} from './candidates.routing';
import {CandidatesHomeComponent} from './components/candidates-home/candidates-home.component';
import { CandidatesImportComponent } from './components/candidates-import/candidates-import.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ExportFromCandidateSourceComponent } from './components/candidates-import/export-from-candidate-source/export-from-candidate-source.component';
import { AdvancedFormsModule } from '@universis/forms';
import { CandidateUploadActionsComponent } from './components/candidate-upload-actions/candidate-upload-actions.component';
import { CandidateUploadActionsHomeComponent } from './components/candidate-upload-actions/candidate-upload-actions-home/candidate-upload-actions-home.component';
import { CandidateUploadActionResultsComponent } from './components/candidate-upload-actions/candidate-upload-action-results/candidate-upload-action-results.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    CandidatesSharedModule,
    CandidatesRoutingModule,
    TablesModule,
    SharedModule,
    FormsModule,
    AdvancedFormsModule,
    MostModule,
    RegistrarSharedModule,
    RouterModalModule,
    StudyProgramsSharedModule,
    StudentsSharedModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    ElementsModule,
    ChartsModule,
    MessagesSharedModule,
    NgxDropzoneModule
  ],
  declarations: [
    CandidatesTableComponent,
    CandidatesHomeComponent,
    CandidatesImportComponent,
    ExportFromCandidateSourceComponent,
    CandidateUploadActionsComponent,
    CandidateUploadActionsHomeComponent,
    CandidateUploadActionResultsComponent
  ],
  entryComponents: [
    SendMessageToStudentComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CandidatesModule { }
