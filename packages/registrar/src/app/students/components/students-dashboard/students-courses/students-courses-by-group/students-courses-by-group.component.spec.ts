import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsCoursesByGroupComponent } from './students-courses-by-group.component';

describe('StudentsCoursesByGroupComponent', () => {
  let component: StudentsCoursesByGroupComponent;
  let fixture: ComponentFixture<StudentsCoursesByGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsCoursesByGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsCoursesByGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
