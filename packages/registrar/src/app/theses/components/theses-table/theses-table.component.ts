import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {
  AdvancedTableComponent,
  AdvancedTableDataResult,
  TableColumnConfiguration
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import { INSTRUCTOR_SHARED_COLUMNS as instructorSharedColumns } from '../../../instructors/components/instructors-table/instructors-shared-columns-config';
import { STUDENT_SHARED_COLUMNS as studentSharedColumns } from '../../../students/components/students-table/students-shared-columns-config';

@Component({
  selector: 'app-theses-table',
  templateUrl: './theses-table.component.html'
})
export class ThesesTableComponent implements OnInit , OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  public recordsTotal: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTableService: ActivatedTableService,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTableService.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = { ... data.searchConfiguration, department: data.department.id };
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Theses.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  onLoading(event: { target: AdvancedTableComponent }) {
    if (event && event.target && event.target.config) {
      // clone shared columns
      let clonedSharedColumns = JSON.parse(JSON.stringify(instructorSharedColumns));
      const configColumns: TableColumnConfiguration[] = event.target.config.columns || [];
      const pushColumn = (column, prefix: string): void => {
        if (!new RegExp(`^${prefix}`).test(column.property)) {
          column.property = `instructor${column.property[0].toUpperCase() + column.property.slice(1)}`;
        }
        // try to find column in the config by name
        // tslint:disable-next-line:max-line-length
        const findColumn: TableColumnConfiguration = configColumns.find((configColumn: TableColumnConfiguration): boolean => configColumn.name === column.name);
        // if it does not exist
        if (findColumn == null) {
          // push it
          configColumns.push(column);
          // add column name to groupBy string if there is one
          if (event.target.config.defaults && event.target.config.defaults.groupBy) {
            event.target.config.defaults.groupBy += `,${column.name}`;
          }
        }
      };

      clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
        if (event.target.config.model === 'StudentTheses') {
          column.name = `thesis/instructor/${column.name}`;
        } else {
          column.name = `instructor/${column.name}`;
        }
        pushColumn(column, 'instructor');
      });

      if (event.target.config.model === 'StudentTheses') {
        clonedSharedColumns = JSON.parse(JSON.stringify(studentSharedColumns));
        clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
          column.name = `student/${column.name}`;
          pushColumn(column, 'student');
        });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}

